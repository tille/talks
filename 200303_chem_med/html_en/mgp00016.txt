
What is Debian?

Linux is just the kernel of your operating system.

You need a lot of applications around.

Those collections of software around the Linux kernel are called distributions.

Companies that build such distributions are called distributors.

They make money by selling their distributions in boxes, doing support and training.

You might know Mandrake, RedHat, SuSE and others.

Debian is just one of them.


