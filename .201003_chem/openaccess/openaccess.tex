\section{Unterschied zwischen materieller Ware und digitaler Information}

Die Frage nach dem Altruismus im Zusammenhang mit Open Access zeigt
deutlich, daß die Eigenschaft "kostenlos" nahezu automatisch mit der
Eigenschaft "selbstlose Hilfe" verbunden wird.  Meiner Meinung nach
liegt die Ursache für diesen Trugschluß in der Annahme, daß es sich
bei digitaler Information um eine Ware handelt.  Diese Annahme ist
jedoch so nicht richtig.  Solange Information an einen materiellen
Träger (Buch) gebunden war und sich nicht ohne materiellen Aufwand
vervielfältigen ließ, durfte man Information in gewissen Grenzen noch
als handelbare Ware betrachten.

Eine Ware setzt zwei Personen in beziehung: Den Veräußerer der Ware
und den Empfänger der Ware.  Sobald der Handel abgeschlossen ist,
verfügt der Veräußerer nicht mehr über die Ware sondern der Empfänger
ist nunmehr der Eigentümer des materiellen Gutes.  Ein und dasselbe
Stück kann genau einmal veräußert werden.  Der Veräußerer erhält in
der Regel einen Gegenwert (Geld) und es handelt sich um einen Verkauf.
Sollte kein Gegenwert vom Veräußerer verlangt werden, so handelt es
sich um ein Geschenk und wird damit als wohlwollende (altruistische)
Handlung bewertet.

Der wesentliche Unterschied beim Veräußern digital vorliegender
Information ist jedoch, daß dieser Vorgang beliebig oft stattfinden
kann, ohne daß der Veräußerer der Information verlustig geht.  Damit
hat der Vorgang einen entscheidenden qualitativen Unterschied
gegenüber dem Veräußern einer materiellen Ware und muß daher auch
unterschiedlich bewertet werden.

Heute werden digitale Inhalte oft künstlich mit der Eigenschaft einer
Ware behaftet (Kopierschutz, um die Information an einen materiellen
Träger zu binden; DRM um die Information für "unrechtmäßige" Empfänger
wertlos zu machen; binden einer Funktionalität an einen materiellen
Gegenstand wie ein Dongle).  Dieses nachträgliche Aufprägen der
Eigenschaften einer materiellen Ware ermöglicht es dem Veräußerer
digitale Information in althergebrachten Schemata zu betrachten.
Dieses Vorgehen läßt potentielle Empfänger glauben, sich nach wie vor
in einer üblichen Verkäufer - Käufer - Beziehung zu befinden und ganz
allgemein wird dieser Zustand als "normal" akzeptiert, weil er den
bekannten und gewohnten Vorgehensweisen entspricht und die neue
Qualität, die digitaler Information innewohnt nicht erkannt wird.
Insofern wird die freie Veräußerung digitaler Information dann als
Geschenk verstanden und möglicherweise altruistische Motive
unterstellt.

Grundsätzlich lassen sich natürlich altruistische Motive nicht
ausschließen und in der Tat läßt sich beobachten, daß sich unter den
Verfechtern der Idee, Information frei zur Verfügung zu stellen auch
viele Idealisten befinden.  Das ändert jedoch nichts an der Tatsache,
daß es sich bei der freien zur Verfügung stellen von Information
keinesfalls um ein Geschenk handelt, sondern um eine neue, der Natur
von digitaler Information innewohnende Verbreitungsart, der lediglich
scheinbar Attribute wie "minderwertig weil kostenlos" und ähnliches
anhaften, weil oben dargelegter Unterschied nicht berücksichtigt wird.

\section{Freie Software}

Bei der ersten digitalen Information, die frei zugänglich gemacht
wurde, handelte es sich um Programmcode.  Genau genommen wurde
Programmcode ursprünglich sogar immer frei zur Verfügung gestellt bis
Computer insbesondere auch durch den PC eine so große Verbreitung
fanden, daß sich ein neues Geschäftsfeld erschließen ließ: Die
nachträgliche Aufprägung von Wareneingenschaften auf digitale
Information und damit die Möglichkeit, diese zu verkaufen.  Dieser
Prozess berührte allerdings nicht jegliche Software: Das UNIX artige
Betriebssystem aus der Berkley Software Distribution (BSD - heute die
Kernkomponente von Mac OSX) oder die durch das GNU Projekt entwickelte
Software, durch die der Hype um das deutlich später entstandene Linux
erst möglich wurde, waren immer frei gewesen und haben wegen dieser
Eigenschaften in spezifischen Bereichen einen herausragenden
Stellenwert erlangt.

Im Fall von Software, die als unmittelbare technische Implementierung
von Wissen und digitale Information mit einer unmittelbaren Funktion
angesehen werden darf, ist der Nutzen, den die freie Verfügbarkeit hat
im Gegensatz zu wissenschaftlichen Publikationen direkt einsehbar:
Der Empfänger der Information kann in den Entwicklungsprozess direkt
eingreifen und dem Programmcode neue Funktionen hinzufühen oder Fehler
beheben, was wiederum durch die Rückkopplung demjenigen, der die
Information zur Verfügung stellt direkt nützt.  Daher kann teilweise
sogar so weit gegangen werden, daß die Freigabe von Programmcode
teilweise schon egoistische Eigenschaften hat:  Ein Programmierer
veröffentlicht unfertigen, mit Fehlern behafteten Programmcode aber in
gewisser Hinsicht für eine Gemeinschaft attraktiven Funktion als
Vorabversion und überläßt der Gemeinschaft das Testen und Suchen von
Fehlern - einem aufwändigen und teueren Qualitätssicherungsprozess für
den auf diese Weise kostenlose "Mitarbeiter" gewonnen werden.  Hier
wird ganz offensichtlich, daß der Code nicht als Geschenk präsentiert
wird, sondern von den Anwendern mit ihrer Arbeitskraft bezahlt wird.

Damit ist natürlich keinesfalls gesagt, daß Freie Software nur
unfertig und fehlerhaft ausgeliefert wird.  Selbst bei als stabil
eingestufter Software ist es für denjenigen stets wertvoll, Wünsche
zur Verbesserung von seinen Nutzern zu erhalten, weil neue Ideen auch
seine eigene Arbeit weiter verbessern helfen.  Es handelt sich um
einen kollektiven Entwicklungsprozess zum Nutzen aller beteiligter.

Es exitistieren natürlich auch Wirtschaftsmodelle, die auf Freier
Software aufbauen: Anwender können Programmierer dafür bezahlen,
spezielle Funktionalitäten zu Software hinzuzufügen oder die Software
an spezielle Gegebenheiten anzupassen.

\section{Freies Wissen}

Eine weitere sehr erfolgreiche Form frei verfügbarer digitaler
Information ist die Sammlung von Wissen, die sich insbesondere in der
WikiPedia manifestiert.  Auch hier ist der Nutzen der Beteiligten sehr
offensichtlich:  Durch die kollektive Arbeit an einer Sammlung des
Wissens mit leichter Zugreifbarkeit wird für denjenigen, der durch
seinen Beitrag andere ebenfalls zur Mitarbeit motivieren kann, deren
Wissen verfügbar.  Hier wurde das erfolgreiche technische Konzept des
freien Programmcodes auf Information ohne unmittelbaren technischen
Nutzen erweitert.  Die Tatsache, daß WikiPedia sich mittlerweile mit
anderen Wissenssammlungen, die kommerziell erstellt wurden messen kann
zeigt, daß das Konzept durchaus erfolgreich ist.

Der Grund für diesen Erfolg ist meiner Meinung nach die Tatsache, daß
die Quantität des heutzutage zur Verfügung stehenden Wissens derart
hoch ist, daß es nur mit einer neuen Qualität der Wissensbereitstellung
erschöpfen zusammengefaßt werden kann.  Es ist also weniger die Frage,
ob Information frei zur Verfügung gestellt wird oder nicht, sondern es
ist vielmehr nur dann möglich, das Wissen der Welt erschöpfend
anzusammeln, wenn dieses auf dem Wege des freien Zugriffs und der
freien Mitarbeit erfolgt.  Auch hier handelt der einzelne Beteiligte
nicht notwendig altruistisch:  Wer die oben genannte These bewußt oder
unbewußt akzeptiert wird zwangsläufig zu der Erkenntnis kommen, daß
nur wenn er sein persönliches Wissen einer Gemeinschaft zur Verfügung
stellt auch vom gesammelten Wissen der Gemeinschaft profitieren kann.

\section{Freies Publizieren / Open Access}

Freie Software und freies Wissen in der WikiPedia haben gegenüber Open
Access einen entscheidenden Vorteil: Es besteht ein mehr oder weniger
direkter Rückkopplungsmechanismus zwischen dem Empfänger der
Information und demjenigen, der sie zur Verfügung stellt.  Letztlich
basiert darauf ein wesentlicher Teil der Motivation für die
Bereitstellung des Wissens.  Meiner Meinung nach mangelt es hier noch
an Mechanismen die dem Publizierenden einen deutlichen Vorteil
gegenüber dem herkömmlichen Publizieren in gedruckten Journalen
verschafft.  Welche Vorteile könnten das sein?

Viele freie Softwareprojecte entstanden, weil die Initiatoren mit
Eigenschaften der entsprechenden proprietären Lösungen unzufrieden
waren.  Analog sollte zunächst Untersucht werden, welche bestehenden
Probleme beim klassischen Publizieren bestehen.  Könnte eventuell
der Vorgang der Rezension verbessert werden und damit die Qualität der
freien Publikation gegenüber der klassischen gesteigert werden?
Voraussetzung dafür ist, daß sich eine Vielzahl kompetenter Experten
zur Verfügung stellt, damit auch sehr spezielle Arbeiten genügend gut
abgedeckt werden.  Welchen Vorteil hätten die Rezensenten darin, ihre
Arbeitskraft zur Verfügung zu stellen.  Eventuell wäre es eine
Möglichkeit, die Rezensenten ebenfalls mit der Publikation zu
verbinden und nicht nur die Liste der an der Publikation
beteiligten. So kann ein Wissenschaftler durch eine Reihe
beachtenswerter Rezensionen ebenfalls seine fachliche Reputation
erhöhen - end möglicherweise attraktiver Lohn für eine geleistete
Arbeit.

weiteres Problem: Freie Software / Wissen lebt von genügend großer
Masse von Mitstreitern, die bei Spezialgebieten naturgemäß nicht
besteht. 
