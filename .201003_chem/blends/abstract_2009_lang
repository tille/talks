Debian Pure Blends

Anpassung von Debian an spezifische Arbeitsumgebungen

Debian Pure Blends (früher unter dem Namen Custom Debian
Distributions) sind nutzerorientierte interne Untermengen der Debian
GNU/Linux Distribution, die spezielle Zielgruppen von Anwendern
unterstützen wollen.  In diesen kommt besonders die Tatsache zum
Ausdruck, daß Debian als Verbindungsglied zwischen Upstream Autor und
Endanwender verstanden werden kann. Die wachsende Bedeutung von Linux
für den Desktop im allgemeinen wird bei Debian durch die Tatsache
reflektiert, daß im vergangenen Jahr einige neue Debian Pure Blends
entstanden sind und die bereits existierenden weitere Fortschritte
verzeichnen konnten.  Die Grundidee der Blends ist es Debian aktiv und
kreativ so zu gestalten, daß es für den angesprochenen Nutzerkreis die
Distribution der Wahl wird.

Zu den Debian Pure Blends gehören z.B. Debian Edu (auch als SkoleLinux
bekannt), Debian Med, Debian Science und andere.

Der Vortrag richtet sich an Zuhörer, die an der Philosophie der Debian
Pure Blends und der dabei eingesetzten Techniken, um die Projekte zu
verwalten, interessiert sind. Es wird im Detail erläutert, warum diese
Projekte keine Abspaltungen ("forks") von Debian sind, sondern
vollständig in die Debian GNU/Linux Distribution integriert sind und
welche Vorteile durch diesen Ansatz erzielt werden.  Darüber hinaus
wird vermittelt, welche Vorteile Projekte erzielen können, die als
Fork von Debian begonnen haben und nun durch schrittweise
Verkleinerung der Unterschiede wieder zu Debian zurückkehren.

Links:
  [1] Vortag auf dem Linux-Info-Tag in Dresden
      http://people.debian.org/~tille/talks/200811_dresden_blends/index_de.html
  Online sichtbare Techniken die vorgestellt werden:
  [2] Exemplarischer Überblick über Tasksseite des Blends Debian Science:
      http://cdd.alioth.debian.org/science/tasks/
  [3] Exemplarischer Überblick über Bugsseite des Blends Debian Junior:
      http://cdd.alioth.debian.org/junior/bugs/

