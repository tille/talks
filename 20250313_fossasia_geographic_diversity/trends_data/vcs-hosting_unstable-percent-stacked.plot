set terminal pdfcairo enhanced color transparent font "Helvetica,8"
set output 'vcs-hosting_unstable-percent.pdf'

set key top left
set key top left at screen 0.23, screen 0.95
set datafile missing '-'
set timefmt "%Y%m%d"
set format x "%m/%Y"
set xdata time
set xrange ["20050101":]
set key autotitle columnheader
set key invert

set ylabel "% of packages"
set style line 1 lc rgb '#fb4934'
set style line 2 lc rgb '#b8bb26'
set style line 3 lc rgb '#fabd2f'
set style line 4 lc rgb '#83a598'
set style line 5 lc rgb '#d3869b'
set style line 6 lc rgb '#8ec07c'
set style line 7 lc rgb '#fe8019'
set style line 8 lc rgb '#cc241d'
set style line 9 lc rgb '#98971a'
set style line 10 lc rgb '#d79921'
set style line 11 lc rgb '#458588'
set style line 12 lc rgb '#b16286'
set style line 13 lc rgb '#689d6a'
set style line 14 lc rgb '#d65d0e'
set style line 15 lc rgb '#9d0006'
set style line 16 lc rgb '#79740e'
set style line 17 lc rgb '#b57614'
set style line 18 lc rgb '#076678'
set style line 19 lc rgb '#8f3f71'
set style line 20 lc rgb '#427b58'
set style line 21 lc rgb '#af3a03'
set arrow from "20230610",graph(0,0) to "20230610",graph(1,1) nohead front lw 1
set label 12 "bookworm" at "20230610",graph(0,0.01) left rotate by 90 front offset -1,0
set arrow from "20210814",graph(0,0) to "20210814",graph(1,1) nohead front lw 1
set label 11 "bullseye" at "20210814",graph(0,0.01) left rotate by 90 front offset -1,0
set arrow from "20190706",graph(0,0) to "20190706",graph(1,1) nohead front lw 1
set label 10 "buster" at "20190706",graph(0,0.01) left rotate by 90 front offset -1,0
set arrow from "20170617",graph(0,0) to "20170617",graph(1,1) nohead front lw 1
set label 9 "stretch" at "20170617",graph(0,0.01) left rotate by 90 front offset -1,0
set arrow from "20150425",graph(0,0) to "20150425",graph(1,1) nohead front lw 1
set label 8 "jessie" at "20150425",graph(0,0.01) left rotate by 90 front offset -1,0
set arrow from "20130504",graph(0,0) to "20130504",graph(1,1) nohead front lw 1
set label 7 "wheezy" at "20130504",graph(0,0.01) left rotate by 90 front offset -1,0
set arrow from "20110206",graph(0,0) to "20110206",graph(1,1) nohead front lw 1
set label 6 "squeeze" at "20110206",graph(0,0.01) left rotate by 90 front offset -1,0
set arrow from "20090214",graph(0,0) to "20090214",graph(1,1) nohead front lw 1
set label 5 "lenny" at "20090214",graph(0,0.01) left rotate by 90 front offset -1,0
set arrow from "20070408",graph(0,0) to "20070408",graph(1,1) nohead front lw 1
set label 4 "etch" at "20070408",graph(0,0.01) left rotate by 90 front offset -1,0

# Define styles for lines with points
#do for [i=1:8] {
#    set style line i lt 1 lw 2 pt i ps 0.5  # Adjust pt and ps here
#}

plot for [i=9:2:-1] "vcs-hosting_unstable.csv" using 1:(100*(sum [col=2:i] column(col))/(sum [col=2:9] column(col))) with filledcurves x1 title columnheader(i) linestyle (i-1)
