#!/usr/bin/python3
import json
import matplotlib.pyplot as plt
import pandas as pd
import sys
import os

def plot_votes_over_time(input_file, output_format='pdf', key='perl-base'):
    """
    Plot the number of votes over time for a given key in the JSON file.

    :param input_file: Path to the input JSON file.
    :param output_format: Format of the output plot file (e.g., 'pdf', 'png').
    :param key: Key in the JSON file to extract data from.
    """
    try:
        # Load the JSON file
        with open(input_file, 'r') as file:
            data = json.load(file)

        # Check if the key exists in the JSON data
        if key not in data:
            raise ValueError(f"Key '{key}' not found in the JSON data.")

        # Extract dates and votes
        dates = list(data[key].keys())
        votes = [details.get('vote', 0) for details in data[key].values()]

        # Create a DataFrame for easier manipulation
        df = pd.DataFrame({'Date': pd.to_datetime(dates), 'Votes': votes})

        # Sort the DataFrame by date
        df = df.sort_values('Date')

        # Set global font settings
        plt.rcParams['font.family'] = 'Liberation Sans'  # Use Liberation Sans
        # plt.rcParams['font.family'] = 'DejaVu Sans'  # Alternative: Use DejaVu Sans
        plt.rcParams['font.size'] = 32  # Base font size (default is 10)
        plt.rcParams['axes.titlesize'] = 24  # Title font size
        plt.rcParams['axes.labelsize'] = 20  # Axis label font size
        plt.rcParams['xtick.labelsize'] = 20  # X-axis tick label font size
        plt.rcParams['ytick.labelsize'] = 20  # Y-axis tick label font size
        plt.rcParams['legend.fontsize'] = 20  # Legend font size

        # Plot the data
        plt.figure(figsize=(16, 9))
        plt.plot(df['Date'], df['Votes'], linestyle='-', marker='.', markersize=1, color='b', linewidth=0.5)
        plt.title(f'Number of Votes for {key} Over Time')
        plt.xlabel('Date')
        plt.ylabel('Votes')
        plt.grid(True)
        plt.xticks(rotation=45)
        plt.tight_layout()

        # Create output file name
        output_file = os.path.splitext(input_file)[0] + f'.{output_format}'

        # Save the plot as a file with a transparent background
        plt.savefig(output_file, transparent=True)
        plt.close()

    except FileNotFoundError:
        print(f"Error: The file '{input_file}' was not found.")
        sys.exit(1)
    except json.JSONDecodeError:
        print(f"Error: The file '{input_file}' contains invalid JSON.")
        sys.exit(1)
    except ValueError as e:
        print(e)
        sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 4:
        print("Usage: python script.py <input_file.json> [output_format] [key]")
        print("Example: python script.py data.json pdf perl-base")
        sys.exit(1)

    input_file = sys.argv[1]
    output_format = sys.argv[2] if len(sys.argv) > 2 else 'pdf'
    key = sys.argv[3] if len(sys.argv) > 3 else 'perl-base'

    plot_votes_over_time(input_file, output_format, key)
