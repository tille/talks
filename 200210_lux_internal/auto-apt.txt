sputnik:~> sudo auto-apt update

put: 880730 files,  1074158 entries
put: 903018 files,  1101981 entries

sputnik:~> auto-apt -x -y run
Entering auto-apt mode: /bin/bash
Exit the command to leave auto-apt mode.
bash-2.05b$ less /usr/share/doc/med-bio/copyright
Reading Package Lists... Done
Building Dependency Tree... Done
The following extra packages will be installed:
  bugsx fastlink readseq 
The following NEW packages will be installed:
  bugsx fastlink med-bio readseq 
0 packages upgraded, 4 newly installed, 0 to remove and 183  not upgraded.
Need to get 0B/1263kB of archives. After unpacking 2008kB will be used.
Reading changelogs... Done
Selecting previously deselected package bugsx.
(Reading database ... 133094 files and directories currently installed.)
Unpacking bugsx (from .../b/bugsx/bugsx_1.08-6_i386.deb) ...
Selecting previously deselected package fastlink.
Unpacking fastlink (from .../fastlink_4.1P-fix81-2_i386.deb) ...
Selecting previously deselected package med-bio.
Unpacking med-bio (from .../med-bio_0.4-1_all.deb) ...
Setting up bugsx (1.08-6) ...

Setting up fastlink (4.1P-fix81-2) ...

Setting up med-bio (0.4-1) ...

localepurge: checking for new locale files ...
localepurge: processing locale files ...
localepurge: processing man pages ...
This package is Copyright 2002 by Andreas Tille <tille@debian.org>

This software is licensed under the GPL.

On Debian systems, the GPL can be found at /usr/share/common-licenses/GPL.
/usr/share/doc/med-bio/copyright (END) 

