Slide 4: "Debian"

   * Debian is assembled by various maintainers all over the world
   * It is transparent which maintainer is maintaining what software
     inside Debian
   * There is a general trend that a software package is maintained
     by a team - so in the majority of cases its not a "single face"
     behind a software package
   * Users [the smiley ;-)] are happy about this concept since there
     is a clear chain of responsibility
   * Debian contains quite common packages like Firefox, Gimp etc.
   * However, there is also quite specific software like WordNet
     (monolingual dicttionary).  The inclusion is driven by the
     maintainer who cares (its the first package of Andreas Tille).
     because Debian accepts any free software as long as it is
     maintained properly and has no release critical bugs
   * The fact that it is possible to include quite specific software
     into a general distribution was unique amongst all large
     distributions at the time when Debian Med started since other
     large distributions were driven by companies who needed to care
     for their market share.  So if you want to plug into a large
     distribution (instead of creating your own distribution) there
     is only one single choice.
   * The integration of Debian Med into a large distribution rather
     than creating one of the countless derivatives is crucial:
     Typically the life time of a topic specific derivative is about
     the time a scientist remains in the same job (=has interest and
     time to do the work).  However, Debian will remain to exist even
     if people who work on a certain topic will come and go.


Slide 5: "Bioinformatics in Debian"

   * Newcomers will have a hard time to find the software inside the
     large package pool.  Debian has more than 40.000 binary packages
     and finding specific tools is like seeking the needle in a
     haystack
   * So what can be done to lead users of medicine and life sciences
     directly to the package they need for they work instead of leaving
     them alone in the chaos of the huge package pool?

... now the "lenses" are flipping in

   * Create a so called "Debian Pure Blend" (in short "Blend") to
     support a specific field of work.  (The slides has the examples
     Skolelinux(=Debian Edu), Debian Med and Debian GIS but there are
     more.)
   * In a Blend a team of experts of the field in question is packaging
     software belonging to this field and provided means to easily
     install this software via so called metapackages.
   * These metapackages are covering certain tasks (in Blend terminology)
     that are covering a specific work field inside the Blend.  For
     instance the Debian Med Blend has a task "Biology" which contains
     biological applications and a task "Biology development" which
     contains development tools to create biological applications.  The
     Debian GIS Blend has a task "OpenStreetMap" containing applications
     that are needed for OSM mapping or running an OSM server.  So each
     Blend has a set of tasks covering different specific purposes of
     the general topic of the Blend.
   * It is important to know that behind the different tasks are usually
     different experts of this specific field - so the software is
     usually packaged by experts who are using and thus testing the
     software on their own.
   * There are also specific web pages (so called tasks pages) which are
     automatically generated giving an overview about the content of
     a task

... at this point I'm usually flipping to a browser and show
      https://blends.debian.org/med/tasks/bio#artemis
    as an example.  The deep link to artemis is helpful since this is the
    first entry featuring a screenshot to show what metainformation is
    available.  Pointing to popcon statistics which names *real* usage,
    inform about the option to translate the descriptions etc. can be
    done if time permits (either now or at the end of the talk


Slide 6: "Med-bio task of Debian Med"

   * The biologist (symbolised by a smiley with DNA shaped eyes and a
     biological application on the computer) just needs to install a single
     package via

         apt-get install med-bio

     (or the prefered apt GUI) to get all the applications relevant for
     biologists and maintained in Debian.
   * So the selection of software becomes very simple for the user and
     remains continuously up to date when a new med-bio package with new
     dependencies will be released by the Debian Med team.
   * All these applications need to comply to the strict quality criterions
     of Debian packages and will be continuously tested to verify their
     functionality (recently a Google Summer of Code project has added
     autopkgtests for the packages with the highest usage numbers - more
     will be added soon)
   * Thus you can find the largest number of ready to install, continuously
     tested software from the field of biology inside a major Linux
     distribution assembled by the Debian Med team into official Debian.


Slide 7: "Packages in selected tasks of Debian Med"

   * Besides the task "Biology" which obviously not only has the highest
     number of packages but also the strongest increase of the number of
     packages over time, there is "Biology Development" which contains for
     instance packages like BioPython, BioPerl etc. to develop biological
     applications.  Those two tasks do contain way more packages than for
     instance "Hospital information systems" (his) or "Medical practice"
     (practice) since the latter are quite dominated by proprietary software
     while biology has some background at universities and is thus more
     strongly tied to Free Software.
   * However, not only the large number of existing Free Software in the
     field of biology has lead to this large increase of the number of
     packages.  Somebody simply has to do the job of packaging.  This was
     made possible by creating an inviting environment to attract software
     authors and users joining the team and doing the work together.
   * The graph quite clearly marks the time span when the work was more
     or less done by 1-3 maintainers and than more and more people joined
     the team up to about 20 very active persons (more to come :-) ).


Slide 8: "Team"

   * I came to this team definition since it happened literally more than
     one time that my problems were solved over night!

