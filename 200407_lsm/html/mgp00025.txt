
Debian-Med in practice

Collection of medical software by meta packages
No research for available medical software necessary
User is not forced to browse the whole package list of Debian
Easy comparison between software covering the same task
Safety against accidental removal of dependencies
Conflicts to incompatible packages
Easy installation
Low effort for administration
Adapted configuration inside meta packages
Care for special needs of Debian-Med users
Pre-configuration regarding certain medical applications
Documentation packages
Packaging and creating relevant documentation
Translation


