
Why not just forking from Debian?

Separate distribution would cost extra effort
Would be hardly better than Debian
Continuous work to keep base system, installer, etc. up to date
Nearly impossible to get security fixes as fast as Debian
Forking would be a bad idea.

Integration into Debian has advantages
Huge user base all over the world thus becoming public on the back of Debian
Secure and stable system without extra effort
Bug Tracking System for free
Infra structure (HTTP-, FTP-, Mailserver, PKI, ...) for free

Bringing back something to Debian
Enhancing quality of packages by making them more user friendly


