Abstract:

The idea of Custom Debian Distributions was born at DebConf 3 in Oslo
and has turned now into a solit toolset that can be used to organise
packages targeting at a specific work field inside Debian in a quite
efficient way.  After five years it is time for a report about status
and success as well as continuing to spread the idea amongst people to
enable them to spend a minimum effort for the adoption of the tools to
get a maximum effect in maintaining a CDD.

--------------------------------------------------------------------

One goal of Custom Debian Distributions is to form a group of Debian
developers who care for a specific set of packages that are used in
the day to day work of a certain user group.  The fact that Debian has
grown to the largest pool of ready to install packages on the net has
led to the side effect that it is hard to maintain for
beginners.  A Custom Debian Distribution adds some substructure
to the currently flat pool of 15000+X packages without a real
structure.  These substructures are intended to put a focus on
special user interest.  These substructures are not oriented on
technical matters like Debian installer team, porting teams or teams
that are focussing to implement programming language policies.

There are some similarities to Debian-i18n which also has the pure
goal to serve the needs of certain end user groups with the difference
that the users are grouped not according to their field of work but
according to their language.  In fact it makes even sense to create
CDDs for languages that require certain technical means to optimally
support the language regarding direction of writing, special fonts
etc.  It is known that some countries in Asia builded Debian
derivatives for this purpose but in principle it is not necessary to
derive - the better solution is to make Debian more flexible by
starting a CDD effort inside Debian.

The talks will give some examples from the success of CDDs like
Debian-Edu and Debian-Med.  One very important outcome of the CDD
effort is the ongoing reunification of Linex - the Debian derived
distribution that is used in all schools in Extremadura - with
Debian-Edu.  This step means that Debian gets a very large
implementation in all schools of Extremadura while on the other hand
the effort of development for the people who invented Linex will be
reduced.  Debian featuring Debian-Edu now has a very good chance to
become a really good international school distribution because it has
roots in five countries (Norway, Spain, France, Germany and Japan) and
might become attractive for many more.

The success stories of CDDs would not have been possible outside
Debian and thus leaving the path to build Zillions of Debian
derivatives that reach a very small user base and working together
inside Debian is the main idea of the talk.  To make this idea more
attractive in the second part of the talk a description of tools that
were developed in the CDD effort will be presented.  Especially the
newly developed web tools that give a good overview about the
packages that are useful for a certain field of work and the QA tools
that enable the CDD team members to easily get an overview about
packages that need some action.  So if people are not yet convinced
that a CDD for their purpose makes sense we will catch them by the
tools they might get for free if they follow the proposed strategy.
