# apt-get install gobby-infinote gobby-0.5 -c gobby.debian.net + debconf11 dc11-debian-med-bof

1 / 16

Debian Med BOF
Andreas Tille
DebConf 11

Banja Luka, 30. July 2011

2 / 16

History

LSM 2001

3 / 16

History

4 / 16

Uploaders of Debian Med team
Andreas T 486 Charles P 324 Steffen M 215 Mathieu M 79 Aaron M. U 70 Steve M. R 66 Dominique B 31 Philipp B 29 Nelson A. de O 20 Jan B 18

0

20

40

60

80

100

2001

2002

2003

2004

2005

2006

2007

2008

2009

2010

2011

5 / 16

People discussing on Debian Med mailing list
Andreas T 2261 Charles P 940 Karsten H 373 David P 305 Steffen M 299 Mathieu M 292 Nelson A. de O 180 Sebastian H 134 Michael H 122 Olivier S 107 400 0 100 200 300

2002

2003

2004

2005

2006

2007

2008

2009

2010

2011

6 / 16

People discussing on Debian Med packaging list
Charles P 457 Andreas T 426 Shaun J 134 Michael H 92 Mathieu M 91 Nelson A. de O 83 Lucas N 74 Steffen M 71 Matthias K 55 LuisAntonio da Sila D 53

0

50

100

150

2006

2007

2008

2009

2010

2011

7 / 16

People commiting to Debian Med SVN or Git
600 Charles P 2217 Andreas T 2085 Steffen M 741 Mathieu M 603 David P 534 Steven M. R 276 olivier s 188 Jan B 121 Nelson A. de O 93 Dominique B 93

0

100

200

300

400

500

2006

2007

2008

2009

2010

2011

8 / 16

Increase of number of packages in selected tasks
Microbiology Imaging Practice

0

20

40

60

80

2002

2003

2004

2005

2006

2007

2008

2009

2010

9 / 16

Covered tasks
Molecular biology, structural biology and bioinformatics
Development of applications Next Generation Sequencing Phylogeny Cloud computing

Drug database Epidemiology Medical imaging / DICOM viewers Medical physics Medical practice Psychological research Neuro Debian Statistics packages which are speciﬁcally designed to be used in medical research or bioinformatics Small tools for several purposes in health care Typesetting and publishing
10 / 16

Not yet / any more covered tasks

Oncology (will be uploaded next month) Content management system speciﬁcally designed for use in medicine was dropped Dental practice was dropped Hospital Information Systems Medical laboratories Pharmacy

11 / 16

People who entered Debian via Debian Med
Steffen Möller Charles Plessy Nelson A. de Oliveira Thijs Kinkhorst David Paleino Tobias Quathamer Michael Hanke Juergen Salk Dominique Belhachemi Mathieu Malaterre Alex Mestiashvili . . . perhaps even more?

12 / 16

Social networking and visibility

Reaching out to a larger user base by using
Debian Med Blog Twitter Identi.ca Facebook Lurk on #debian-med IRC channel ???

Visibility enhancements
Logo Flyer ???

13 / 16

There will be no problems!

..

We are ﬁghting through the dependency jungle of Java applications Getting large applications like VistA packaged Working out non-free issues Install / Live CD ? Always feel free to work on our TODO list in Wiki In case you just want to ﬁx some bugs go here

14 / 16

This talk is available at http://people.debian.org/˜ tille/talks/ Andreas Tille <tille@debian.org>

