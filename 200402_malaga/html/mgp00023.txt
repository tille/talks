
Technology (1): Meta Packages

Collection of specific software 
No research for available software necessary
User is not forced to browse the whole package list of Debian
Easy comparison between software covering the same task
Safety against accidental removal of dependencies
Conflicts to incompatible packages
Easy installation
Low effort for administration

Adapted configuration inside meta packages
Care for special needs of project users

Documentation packages
Packaging and creating of relevant documentation
Translation


