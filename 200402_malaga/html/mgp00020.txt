
Debian Accessibility Project


Debian for blind and visually impaired people

Debian accessible to people with disabilities
Special care for
Screen readers
Screen magnification programs
Software speech synthesizers
Speech recognition software
Scanner drivers and OCR software
Specialized software like edbrowse (web-browse in the spirit of line-editors)
Making text-mode interfaces available
Providing screen reader functionality during installation

