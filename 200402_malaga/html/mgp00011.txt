
History of Custom Debian Distributions

Debian-Junior
Start: beginning of 2000
Initiator: Ben Armstrong <synrg@debian.org>
First Custom Distribution - in fact the idea was born
Released in the current stable Debian code name Woody

Debian-Med
Start: beginning of 2002
Initiator: Andreas Tille <tille@debian.org>
Adapted ideas from Debian-Junior and added some stuff

Debian-Edu
Start: end of 2002
Initiator: Rapha�l Hertzog <hertzog@debian.org> --> Skolelinux
Bringing back a fork from Debian

Demudi
Start: not really started yet 
Initiator: Marco Trevisani <marco@centrotemporeale.it>
Bringing back a fork from Debian

