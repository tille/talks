# LaTeX2HTML 2008 (1.71)
# Associate images original text with physical files.


$key = q/{description*}item[tasksel]Provisionofareasonablesoftwareselectionforrathergenerertainpackagesandtodisplaydetailedinformationabouteachpackage.{description*};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="556" HEIGHT="244" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="\begin{description*}
\item[tasksel] Provision of a reasonable software selection...
...ages and to display detailed information about each
package.
\end{description*}">|; 

$key = q/{itemize*}itemPreparingacollectionofthesoftwareforthetargetdomainofusenicelyintemediaandprinteddocumentation.itemOfferingtrainingandqualification.{itemize*};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="531" HEIGHT="168" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="\begin{itemize*}
\item Preparing a collection of the software for the target do...
...rinted documentation.
\item Offering training and qualification.
\end{itemize*}">|; 

$key = q/{itemize*}itemeasytoassembleitemtailoredforthepersonalneedsofthisspecificworkgroupitemattractivewebappearance{itemize*};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="382" HEIGHT="53" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="\begin{itemize*}
\item easy to assemble
\item tailored for the personal needs of this specific work group
\item attractive web appearance
\end{itemize*}">|; 

$key = q/{itemize*}itemsloppyaboutpolicyoftheunderlyingDebiansystemitemsloppyaboutthelicensesoftheincludedprograms{itemize*};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="352" HEIGHT="34" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="\begin{itemize*}
\item sloppy about policy of the underlying Debian system
\item sloppy about the licenses of the included programs
\end{itemize*}">|; 

1;

