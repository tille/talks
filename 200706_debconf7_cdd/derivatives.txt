                                             English ? Spanish ? French ?
                                             German ? Ukrainian ? Japanese ?
                                             Arabic

                                              ? Home
                                               Page
DistroWatch.com                                ? DW     ? Search      ? About
[Type Distribution Na] [Select Distribution] Weekly,     ? Major    DistroWatch
[Go]                   [Go]                  Comments Distributions    ? FAQ
                                                ?       ? Submit    ? Advertise
                                             Reviews  Distribution   ? Related
                                               and     ? Upcoming      Links
                                             Articles   Releases
                                                ?
                                             Packages




Distribution Statistics - Independence


                                 Introduction
The number of distributions in this site's database is quite impressive --
until you realise that the majority of them is nothing but modified versions of
Debian GNU/Linux or Fedora Core/Red Hat Linux. In some cases these modified
distributions can be justified; a distribution offering a better localisation
support for a language-specific market is a good example. Some distributions,
such as Mandrakelinux, Conectiva and PLD were originally based on Red Hat,
although they have since developed into distributions in their own rights and
there are now other distributions based on them. As for SUSE, a reader has
pointed out that it was originally based on a German distribution called Jurix,
which is no longer maintained. Some distributions are listed twice in the table
- this is the case of those distributions that descended from a parent which
itself is based on another parent (many Knoppix-based distributions are
therefore listed under both Debian and Knoppix). Alphabetical list: Damn Small
Linux ? Debian GNU/Linux ? Fedora Core ? FreeBSD ? Gentoo Linux ? Knoppix ?
Kurumin Linux ? Linux From Scratch ? Mandrakelinux ? Morphix ? Red Hat
Enterprise Linux ? Slackware Linux ? SUSE LINUX ? Other distributions.


         Distribution Statistics - Independence
Sponsored Message
2X ApplicationServer for Windows Terminal Services

Debian Based on Debian GNU/Linux: 129 Distributions

       Abul�du ? Adamantix ? AGNULA GNU/Linux Audio
       Distribution ? Amber Linux ? ANTEMIUM Linux ?
       Arabbix ? ARMA aka Omoikane GNU/Linux ? ASLinux ?
       Auditor Security Linux ? Augustux ? B2D Linux ?
       BeatrIX Linux ? BEERnix ? Biadix ? BIG LINUX ?
       Bioknoppix ? BlackRhino ? Bluewall GNU/Linux ?
       Bonzai Linux ? BrlSpeak ? C�tix ? CensorNet ?
       Clusterix ? ClusterKNOPPIX ? Condorux ? Damn
       Small Linux ? Danix ? DeadCD ? DebXPde ? Dizinha
       Linux ? eduKnoppix ? ERPOSS ? ESware ? Evinux ?
       Euronode ? FAMELIX ? Feather Linux ? Flonix ?
       Vital Data Forensic or Rescue Kit (FoRK) ?
       Freeduc-cd ? Freeduc-Sup ? GEOLivre Linux ?
       Gibraltar Firewall ? GNIX-Vivo ? Gnoppix Linux ?
       gnuLinEx ? GNU/Linux Kinneret ? GNUstep Live CD ?
       grml ? Guadalinex ? Helix ? Hikarunix ? Hiweed
       Linux ? Impi Linux ? IndLinux ? Julex ? K-DEMar ?
       Kaella ? Knoppix Linux Azur ? Kalango Linux ?
       KANOTIX ? KlusTriX ? knopILS ? Knoppel ? Knoppix
Debian ? Knoppix 64 ? Knoppix STD ? KnoppiXMAME ?
       KnoppMyth ? KnoSciences ? Kurumin Linux ? LAMPPIX
       ? Libranet GNU/Linux ? LIIS Linux ? LinEspa ?
       Linspire ? Linux Live Game Project ? Linux Loco ?
       LinuxDefender Live! CD ? Linuxin ? LiVux ? Local
       Area Security Linux (L.A.S.) ? Luinux ? Luit
       Linux ? MAX: Madrid_Linux ? Mediainlinux ? MEPIS
       Linux ? Metadistro-Pequelin ? MIKO GNYO/Linux ?
       MoLinux ? Morphix ? Munjoy Linux ? Nature's Linux
       ? NordisKnoppix ? OGo Knoppix ? Oralux ?
       Overclockix ? Quantian ? PaiPix ? ParallelKnoppix
       ? Parsix GNU/Linux ? Penguin Sleuth ? PHLAK ?
       PilotLinux ? Progeny Debian ? Rays Linux ?
       ROSLIMS Live CD ? Salvare ? Santa Fe Linux ?
       Skolelinux ? Slavix ? Slix ? Slo-Tech Linux ?
       Soyombo Mongolian Linux ? SphinxOS ? Tablix on
       Morphix ? Tilix Linux ? TupiServer Linux ? Ubuntu
       Linux ? UserLinux ? WHoppiX ? X-evian ? Xfld ?
       Xandros Desktop OS ? Xarnoppix ? Zen Linux ?
       ZoneCD ? Zopix


Fedora Based on Fedora Core/Red Hat Linux: 63
       Distributions

       ADIOS Linux Boot CD ? Asianux ? Aurora SPARC
       Linux ? Aurox Linux ? ASP Linux ? Basilisk Live
       CD ? Bayanihan Linux ? Berry Linux ? BioBrew
       Linux Distribution ? BLAG Linux And GNU ? Chinese
       2000 Linux ? Chinese Linux Extension ?
       ClarkConnect Broadband Gateway ? Cobind Desktop ?
       Cool Linux ? Cosix Linux ? CPUBuilders Linux ?
       ELX Linux ? EnGarde Secure Linux ? FoX Linux ?
       Freedows ? FTOSX Desktop ? Gelecek Linux ?
       Haansoft Linux ? Hakin9 Live ? Hancom Linux ?
       Haydar Linux ? HispaFuentes Linux ? Holon Linux ?
Fedora IDMS Linux ? Ignalum Linux ? K12 Linux Terminal
       Server Project ? KoreLinux ? KRUD ? LBA-Linux ?
       Linare Linux ? Linpus Linux ? Linux Media Lab
       Distribution ? Linux XP ? LinuxTLE ? Lorma Linux
       ? Magic Linux ? MIZI Linux ? MSC.Linux ?
       OpenDesktop ? OpenNA Linux ? Openwall GNU/*/Linux
       ? PHP Solutions Live CD ? Plan-B ? Red Flag Linux
       ? redWall Firewall ? Resala Linux ? Rocks Cluster
       Distribution ? SME Server ? TFM Linux ? Thiz
       Linux ? tinysofa enterprise linux ? Trustix
       Secure Linux ? Turbolinux ? Vine Linux ? WOW
       Linux ? Xteam Linux ? Yellow Dog Linux


Knoppix Based on Knoppix: 50 Distributions

        Abul�du ? Auditor Security Linux ? Arabbix ?
        BEERnix ? BIG LINUX ? Bioknoppix ?
        clusterKNOPPIX ? Condorux ? Damn Small Linux ?
        Danix ? LinuxDefender Live! CD ? eduKnoppix ?
        Evinux ? Feather Linux ? Vital Data Forensic or          Search
        Rescue Kit (FoRK) ? Freeduc-cd ? Julex ? Helix ? Google
        K-DEMar ? Kaella, Knoppix Linux Azur ? GNU/Linux [                    ]
        Kinneret ? KlusTriX ? knopILS ? Knoppel ?        ( ) Web (*)
Knoppix Knoppix 64 ? Knoppix STD ? KnoppMyth ?           DistroWatch.com
        KnoSciences ? Kurumin Linux ? Local Area         [Search] 
        Security Linux (L.A.S.) ? MAX: Madrid_Linux ?
        Mediainlinux ? MEPIS Linux ? Munjoy Linux ?
        NordisKnoppix ? OpenGroupware.org Knoppix CD ?   Advertisement
        Oralux ? Overclockix ? PaiPix ? ParallelKnoppix
        ? Pardus Live CD ? Parsix GNU/Linux ? Penguin
        Sleuth ? Metadistro-Pequelin ? Quantian ?        Amazon.de
        ROSLIMS Live CD ? Slix ? Tilix Linux ? WHoppiX ?
        Xarnoppix ? X-evian ? Zopix


Independent Independently Developed: 29 Distributions
                                                              LinuxCD.org
            ALT Linux ? Arch Linux ? Ark Linux ? cAos ?
            CCux Linux ? CRUX ? Debian GNU/Linux ?       LinuxCD.org
            dyne:bolic ? Fedora Core ? Freepia ? Gentoo  1000+ versions Linux &
            Linux ? GoboLinux ? Linux From Scratch ?     BSD
            Litrix ? MandrakelinuxMomonga Linux ? Nitix
            Autonomic Linux ? Octoz GNU/Linux ? Onebase    CentOS 5.0 ($5.25)
            Linux ? Peanut Linux ? Project dEv ? Puppy     Debian 4.0 ($15.75)
            Linux ? QiLinux ? ROCK Linux ? Server          Fedora 6 ($5.25)
            optimized Linux ? Slackware Linux ? Sorcerer   FreeBSD 6.2 ($3.50)
            ? Specifix Linux ? SUSE LINUX ? UHU-Linux      Gentoo 2007.0
                                                           ($1.95)
                                                           KNOPPIX 5.1.1
Slackware Based on Slackware Linux: 28 Distributions       ($1.75)
                                                           Kubuntu 7.04 ($1.75)
          AUSTRUMI ? BearOps Desktop Linux OS ? Blin       Mandriva 2007.1
          Linux ? Burapha Linux ? Buffalo Linux ?          ($5.25)
          CollegeLinux ? DARKSTAR Linux ? Definity Linux   openSUSE 10.2
          ? DeLi Linux ? DNALinux ? Drinou-Linux ?         ($5.25)
          eLearnix ? Frugalware Linux ? gNOX ? GoblinX ?   PCLinuxOS 2007
Slackware JoLinux ? Linux LiveCD Router ? Minislack ?      ($1.75)
          MoviX ? Netwosix ? OpenLab Community Edition ?   Scientific 5.0
          Plamo Linux ? Recovery Is Possible ? ROOT GNU/   ($5.75)
          Linux ? RUNT: The ResNet USB Network Tester ?    SimplyMEPIS 6.5
          SLAX Linux Live ? STUX GNU/Linux ? Vector        ($1.75)
          Linux                                            Ubuntu 7.04 ($1.75)

                                                         10% discount for
Mandrakelinux Based on Mandrakelinux: 14 Distributions   returning customers
                                                         Click here to visit 
              Annvix ? APODIO ? Ayrsoft icon ?           LinuxCD.org now
              blackPanther OS ? EduLinux ? HKLPG Linux ?
Mandrakelinux LinuxConsole ? LINUXO Live! ? Mandows ?
              MCNLive CD ? PCLinuxOS ? SAM Mini-Live-CD
              ? vnlinuxCD ? Turkix


LFS Based on Linux From Scratch: 12 Distributions

    The Athene Operating System ? Core Linux ?
LFS Devil-Linux ? IPCop Firewall ? Lonix ? Murix Linux ?
    Nasga�a GNU/Linux ? Phayoune Linux ? Sentinix ?
    TA-Linux ? Yoper Linux ? ZENIX GNU/Linux


Red Based on Red Hat Enterprise Linux: 12 Distributions
Hat

    CentOS ? Eadem Enterprise AS ? Fermi Linux ? Lineox
Red Enterprise Linux ? Miracle Linux ? NuxOne Linux ?
Hat Pie Box Enterprise Linux ? Scientific Linux ?
    StartCom Linux ? Tao Linux ? White Box Enterprise
    Linux ? X/OS Linux


Gentoo Based on Gentoo Linux: 12 Distributions

       Flash Linux ? Gentoox ? iBox ? Jollix ?
Gentoo Knopperdisk ? Navyn OS ? Pardus Live CD ? Shark
       Linux ? SystemRescueCd ? Ututo ? Vidalinux
       Desktop OS ? ZerahStar Zesktop


Morphix Based on Morphix: 12 Distributions

        Amber Linux ? Ankur Bangla Live! Desktop ?
        Arabbix ? Clusterix ? Freeduc-Sup ? GNIX-Vivo ?
Morphix GNUstep Live CD ? IndLinux ? Slavix ? Slo-Tech
        Linux ? Soyombo Mongolian Linux ? Tablix on
        Morphix


Damn  Based on Damn Small Linux: 6 Distributions
Small

Damn  Flonix ? Hikarunix ? INSERT - Inside Security
Small Rescue Toolkit ? JUSIX ? LAMPPIX ? Luit Linux


FreeBSD Based on FreeBSD: 5 Distributions

FreeBSD DragonFly BSD ? FreeSBIE ? GuLIC-BSD ? m0n0wall
        ? TrianceOS


Kurumin Based on Kurumin Linux: 5 Distributions

Kurumin Dizinha Linux ? FAMELIX ? GEOLivre Linux ?
        Kalango Linux ? TupiServer Linux


SUSE Based on SUSE LINUX: 5 Distributions

SUSE Linux Caixa M�gica ? kmLinux ? Novell Linux Desktop
     ? stresslinux ? Sun Java Desktop System


Other Based on Other Distributions

      ? Aurox Linux: Hakin9 Live ? Linux+ Live ? PHP
      Solutions Live CD
      ? Progeny Debian: Linux Loco ? MoLinux
      ? SLAX Linux Live: DNALinux ? Litrix Linux
      ? Sorcerer: Lunar Linux ? Source Mage GNU/Linux
      ? Ubuntu Linux: BeatrIX Linux ? Gnoppix Linux
      ? Arch Linux: AL-AMLUG Live CD
      ? Berry Linux: SULIX
      ? Caldera OpenLinux: Lycoris Desktop/LX
      ? Conectiva Linux: Tech Linux
      ? Gnoppix Linux: Impi Linux
      ? Holon Linux: Happy MacLinux
      ? MEPIS Linux: SphinxOS
      ? OpenBSD: MirOS
      ? Peanut Linux: Boten Linux
      ? ROCK Linux: T2
      ? SME Server: YES Linux


-------------------------------------------------------------------------------

Copyright � 2001 - 2007 DistroWatch.com. All rights reserved. All trademarks
are the property of their respective owners. DistroWatch.com is hosted at Green
Bay and mirrored at Dallas, Kiev, Praha, Timisoara and Wien.
Contact, corrections and suggestions: Ladislav Bodnar.
--
DistroWatch.com is hosted by NetSonic.net. If you need a reliable Debian/Fedora
/FreeBSD/Red Hat web hosting solution with excellent support, we highly
recommend NetSonic.net.

