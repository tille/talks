
Was sind Custom Debian Distributions?
Debian besteht aus etwa 10000 Programm-Paketen.
In der Regel sind Nutzer jedoch nur an einer Untermenge all dieser Pakete interessiert.
Custom Debian Distributions sorgen f�r spezielle Nutzergruppen mit unterschiedlichen F�higkeiten und Interessen.
Es sind nicht nur praktische Sammlungen von spezifischen Programm-Paketen sondern es wird auch f�r leichte Installation und Konfiguration Sorge getragen.
Keine Abspaltung ("fork") von Debian
Grundidee: Es wird keine separate Distribution erstellt, sondern Debian f�r einen speziellen Zweck angepa�t.

