#!/usr/bin/python3
import json
import matplotlib.pyplot as plt
import pandas as pd
import sys
import os

def plot_votes_over_time(input_file):
    # Load the JSON file
    with open(input_file, 'r') as file:
        data = json.load(file)

    # Extract dates and votes
    dates = []
    votes = []
    for date, details in data['perl-base'].items():
        dates.append(date)
        votes.append(details.get('vote', 0))

    # Create a DataFrame for easier manipulation
    df = pd.DataFrame({'Date': pd.to_datetime(dates), 'Votes': votes})

    # Sort the DataFrame by date
    df = df.sort_values('Date')

    # Plot the data
    plt.figure(figsize=(15, 10))
    plt.plot(df['Date'], df['Votes'], linestyle='-', marker='.', markersize=1, color='b', linewidth=0.5)
    plt.title('Number of Votes for perl-base Over Time')
    plt.xlabel('Date')
    plt.ylabel('Votes')
    plt.grid(True)
    plt.xticks(rotation=45)
    plt.tight_layout()

    # Create output file name
    output_file = os.path.splitext(input_file)[0] + '.pdf'

    # Save the plot as a PDF file with a transparent background
    plt.savefig(output_file, transparent=True)
    plt.close()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <input_file.json>")
        sys.exit(1)

    input_file = sys.argv[1]
    plot_votes_over_time(input_file)
