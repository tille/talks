
Why use Debian for medical care?


Focus on security and stability
Powerful packaging tools
Strong quality assurance, carefully tested
Strict rules (policy)
Support of 11 hardware architectures (auto builders: alpha, arm, hppa, i386, ia64, m68k, mips, mipsel, powerpc, s390, sparc)
Developed by about 1000 volunteers
Single developers have influence on development - they just have to do it


