
Debian-Edu

Debian for educational purposes

Make Debian the best distribution available for educational use
Federate many initiatives around education (which are partly based on forks of Debian)
Integrate changes from the French Debian Education distribution into Debian
SkoleLinux people recently took over maintenance
Focus on easy installation in schools
Cooperate with other education-related projects (like Seul, Ofset, KdeEdu)

