\documentclass[compress,aspectratio=169]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{newunicodechar}
\usepackage{gensymb}
\usepackage{changepage}

\mode<presentation>
{
  \usetheme{Wernigerode}
  %\AtBeginSubsection[]{}
}

\usepackage{debian-at}
\usepackage{debian-freebeer}
\usepackage{bioapplications}

\title{Teams, newcomers and numbers}

\author{Andreas Tille}
\institute{Debian}

\date{Kochi, 10. September 2023}

% https://debconf23.debconf.org/talks/32-teams-newcomers-and-numbers/
%
% Teams in Debian are somehow in flux. In many teams it is not fully
% clear who is currently contributing or has moved to some other tasks
% while newcomers might be at the doorstep but have trouble to enter.
% In teammetrics some numbers where gathered. While these give some
% hints about the structure of a team, like how many people are
% effectively contributing and how good the workload is shared between
% the team members, it also enables interesting insight into the release
% process in general. This talk tries to present some conclusion from
% the numbers I've drawn specifically for the teams I'm working in.
% I will also discuss the problem, that in the freeze time before each
% release the overall activity will decrease.
%  
% Date: 	2023-09-10
% Time: 	15:30:00
% Duration: 	00:45:00
% Room: 	Kuthiran

\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{}

  \begin{itemize}
    \item Originally packages were literally {\em owned} by their maintainer
    \item At DebConf3 Teams were suggested, since DebConf5 Uploaders
    \item Both quickly adopted in Debian Med and other Blends
    \item At DebConf8 I realised: Hard to specify who is in the team
    \item Solved for DebConf11 with GSoC Teammetrics
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Types of teams}

  \begin{itemize}
    \item Content driven teams - frequently Blends
    \item Infrastructure teams (installer, dpkg, apt)
    \item Language teams
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Composition of teams}

  \begin{itemize}
    \item Well staffed, more than one member can handle issues
    \item Multiple members but reaching limits
    \item Growing teams
    \item Shrinking teams
    \item (Basically) one member team
    \item Single maintainers
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Culture in teams}

  \begin{itemize}
    \item Every member can and should upload every team package
    \item Few members care for everything, others only for specific packages
    \item Uploader cares for specific packages, others will contribute randomly
    \item Team wide automatic packaging upgrades in Git (Janitor)
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Debian Med has attracted one developer per year}
  According to a \link{http://wiki.debian.org/DebianMed/Developers}{survey} in Wiki
  \begin{itemize}
    \item Debian Med has 45 DDs+DMs (not all active any more)
    \item 23\,DDs {\em because} Debian Med exists
    \item 18 out of the 23 above extended their activity to other
          fields in Debian
    \item 13 out of the 23 above are currently active in Debian Med
    \item 3\,DDs and 1\,DM are new after the April-Hackathon
  \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Actively inviting and teaching}
    
    \begin{itemize}
      \item Mentoring of the Month (teaching upstream and users)
      \item Sprints in common with upstream and users
      \item Participating in GSoC + Outreachy
      \item Sponsoring of Blends
    \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Mentoring of the Month}

  \begin{itemize}
    \item MoM is {\em work}
    \item hopefully it shows that this work is also {\em fun}
    \item mentor trades his spare time for the work of the student
    \item attempt to train "silent observers on the list"
    \item focus not only on technical details but also on learning to
          know relevant communication channels
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{MoM Conclusions}

  \begin{itemize}
    \item Time spent into mentoring is worth the effort
    \item No student for each month so the workload is bearable
    \item Students have just read recent documents which I did ten years
          ago \RightArrow I can learn new stuff from them
    \item Major advantage: training upstream to pool their knowledge about
          the code with ours about packaging is {\bf very efficient} for
          the hard packages
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{GSoC + Outreachy}

  \begin{itemize}
    \item Provide task for every GSoC + Outreachy project Debian participates
    \item Really good experiences with Outreachy students
    \item Very good fitting task: Write autopkgtests
    \item List of packages without autopkgtest sorted by popcon
    \item Just work down the list and add tests
  \end{itemize}

\end{frame}


{% This bracket pair is important to not spoil output after these slides!
% This has no effect
\setbeamercolor{background canvas}{bg=white}
\setbeamercolor{normal text}{bg=red!12}
% this changes also bottom line as well as the following slides
\setbeamertemplate{background canvas}[vertical shading][top=white,bottom=white]
\FreeBeerDebianShortSechzehnNeun%
\FreeBeerBlendsSechzehnNeun%
{\vbox to 2ex{\vss\vbox to 1ex{\hbox to \hsize{\small Role of Blends to attract specific users\hfill}}\vfill}}
\BioDistributionSechzehnNeun%
\setbeamertemplate{background canvas}[vertical shading][top=blue!20!black,bottom=blue!90!yellow]
}

\begin{frame}
  \frametitle{Sponsoring of Blends (SoB)}
  \begin{itemize}
    \item Blends concept remains widely unknown amongst newcomers
    \item Newcomers might desperately seek for sponsors and simply do not know
          how to find one
    \item Kill two birds with one stone:  Get the package sponsored after proving
          that you understood the Blends techniques
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{SoB rules}
  \begin{enumerate}
    \item You confirm to have understood the Blends principle (have read the Blends
          documentation, are member of some team on alioth, reading the relevant
          mailing list)
    \item Your package is maintained in Salsa in the according Blends team space
    \item Your package is listed on the Blends tasks pages (which is either because
          the package is inside Debian or in any Blends Git)
    \item You are unable to find a sponsor on the specific Blends list even after
          posting there at least twice 
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Top 10 Uploaders of Debian Med team}
  \hbox to \hsize{%
   \includegraphics[width=0.95\textwidth,height=0.8\textheight]{uploaders_debian-med}%
  \hfill}
\end{frame}

\begin{frame}
  \frametitle{Top 10 bug hunters of Debian Med packages}
  \hbox to \hsize{%
   \includegraphics[width=0.95\textwidth,height=0.8\textheight]{bugs_debian-med}%
  \hfill}
\end{frame}

\begin{frame}
  \frametitle{Top 10 commiters to Debian Med VCS}
  \hbox to \hsize{%
   \includegraphics[width=0.95\textwidth,height=0.8\textheight]{commitstat_debian-med}%
  \hfill}
\end{frame}

\begin{frame}
  \frametitle{Maintainer per package relation in Debian Med}
  \hbox to \hsize{%
   \includegraphics[width=0.95\textwidth,height=0.8\textheight]{maintainer_per_package_debian-med}%
  \hfill}
\end{frame}

\begin{frame}
  \frametitle{Comparing team coverage of their packages}
  \begin{tabular}{p{0.47\textwidth}p{0.47\textwidth}}
    \includegraphics[width=0.47\textwidth]{maintainer_per_package_debian-science.pdf} & 
    \includegraphics[width=0.47\textwidth]{maintainer_per_package_pkg-perl.pdf} \\
  \end{tabular}
\end{frame}


\begin{frame}
  \frametitle{Inviting environment}

In several talks at DebConf and in discussions before I have heard the
argument that it is hard to find friends.  But it is not.  The answer is
to create inviting teams.

\end{frame}

\begin{frame}
  \frametitle{Team}
  \begin{itemize}
    \item[] \goal{Waking up in the morning and realising \\
                  that somebody else has solved your \\
                  problem from yesterday}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{In case you want to run your own team stats}

  \link{https://salsa.debian.org/teammetrics-team/teammetrics/-/blob/master/admin/download_database.sh}{There is a download script}

  \hbox to \hsize{\hfil%
   \includegraphics[width=0.9\textwidth,height=0.75\textheight]{download_database.png}%
  \hfil}

  The database enables you to do more fine grained investigation of your team

\end{frame}


\begin{frame}
  \frametitle{Persons commiting to Debian Med packages per week}
%  \vspace*{-5mm}
  \hbox to \hsize{%
   \includegraphics[width=0.95\textwidth,height=0.8\textheight]{commiters}%
  \hfill}
\end{frame}

\begin{frame}
  \frametitle{Commits to Debian Med packages per week}
%  \vspace*{-5mm}
  \hbox to \hsize{%
   \includegraphics[width=0.95\textwidth,height=0.8\textheight]{commits}%
  \hfill}
\end{frame}

\begin{frame}
  \frametitle{Checking the theory of general "freeze depression"}
  \begin{itemize}
    \item Graph over all teams in team metrics
    \item Start of freeze is marked by transition freeze (if exists)
    \item Some kind of artificial peaks (for instance in week 2021-04-12
          with 3592 commits by pkg-js team)
          % SELECT project, count(*) FROM (SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start, project FROM commitstat) zw  WHERE week_start = '2021-04-12' GROUP BY project ORDER BY count;
    \item There are other peak examples outside freeze (for instance in
          week 2020-02-17 with 7315 commits by pkg-perl team)
          % SELECT project, count(*) FROM (SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start, project FROM commitstat) zw  WHERE week_start = '2020-02-17' GROUP BY project ORDER BY count DESC;
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Commits of all teams verifying "freeze depression"}
%  \vspace*{-5mm}
  \hbox to \hsize{%
   \includegraphics[width=0.95\textwidth,height=0.8\textheight]{commits-all}%
  \hfill}
\end{frame}

\begin{frame}
  \frametitle{Uploads of all teams in teammetrics packages per week}
%  \vspace*{-5mm}
  \hbox to \hsize{%
   \includegraphics[width=0.95\textwidth,height=0.8\textheight]{uploads-all}%
  \hfill}
\end{frame}

\begin{frame}
  \frametitle{Pre-freeze activity}
\begin{itemize}
    \item Right before the freeze in January 2021 three teams did a lot of uploads right before
          the freeze (pkg-perl 916, ruby-extras 240, pkg-go 210)
% SELECT maintainer_email, count(*) FROM (SELECT to_char(date_trunc('week', date), 'YYYY-MM-DD') AS week_start,
%       maintainer_email
%    FROM upload_history
%    WHERE source IN (SELECT DISTINCT source FROM sources)) zw WHERE week_start = '2021-01-04' GROUP BY maintainer_email ORDER BY count DESC LIMIT 3;
%                  maintainer_email                   | count.
%-----------------------------------------------------+-------
% pkg-perl-maintainers@lists.alioth.debian.org        |   916
% pkg-ruby-extras-maintainers@lists.alioth.debian.org |   240
% pkg-go-maintainers@lists.alioth.debian.org          |   210
    \item Similarly in Debian Med team with the all time peak some weeks before the freeze as mentioned above
\end{itemize}
\end{frame}



{\setbeamertemplate{background}{\includegraphics[width=\paperwidth]{20180117_075005_ilce-7_dsc06271_.jpg}}%
\definecolor{LinkColor}{rgb}{0.1, 0.1, 0.5}
\definecolor{MailColor}{rgb}{0.95, 0.95, 1}

\frame<handout:0>[plain]{
%\hspace*{14mm}\Large\color{LinkColor}This talk is available at
%\hspace*{7mm}\href{http://people.debian.org/\~tille/talks/}{http://people.debian.org/\~\,tille/talks/}
\Large\color{LinkColor}Slides will be on my talks page later

\vspace{70mm}
\Large\color{yellow}\hspace{30mm}\href{mailto:tille@debian.org}{Andreas Tille <tille@debian.org>}
}
}

\frame<0| handout:1>[plain]{
This talk is available at\newline
\href{http://people.debian.org/\~tille/talks/}{http://people.debian.org/\~\,tille/talks/}\newline
\href{mailto:tille@debian.org}{Andreas Tille <tille@debian.org>}\newline
}


\end{document}

