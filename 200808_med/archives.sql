#!/bin/sh

createdb listarchives

psql listarchives <<EOT

BEGIN;

CREATE TABLE listarchive (
   project   text,
   yearmonth date,
   author    text,
   subject   text,
   url       text,
   ts        date
);

CREATE LANGUAGE plpgsql ;

/*
 * Build a query string for several purposes
 *   ARG1: Query to obtain wanted columns
 *   ARG2: Feature that is queried
 * See below how this helper is used.
 */
CREATE OR REPLACE FUNCTION BuildQueryCDDsHelper(text, text)
    RETURNS text AS '
    DECLARE
       IterQuery  ALIAS FOR \$1 ;
       Feature    ALIAS FOR \$2 ;
       ret        text ;
       union      text ;
       query1     text ;
       query2     text ;
       r1         RECORD ;
       r2         RECORD ;
       ri         RECORD ;
       qi         RECORD ;
    BEGIN

    query1 := IterQuery ;
    query2 := query1;
    union  := '''' ;
    ret    := ''SELECT yearmonth'' ;

    FOR r1 IN EXECUTE query1 LOOP
    	ret := ret || '', CAST(SUM("'' || r1.feature || ''") AS int) AS "'' || r1.feature || ''"'' ;
    END LOOP;

    ret := ret || ''
  FROM (
'' ;

    FOR r1 IN EXECUTE query1 LOOP
       ret   := ret || union || ''    SELECT yearmonth'' ;
       union := ''
    UNION
'';
       FOR r2 IN EXECUTE query2 LOOP
       	   IF r1.feature = r2.feature THEN
	      ret := ret || '', COUNT(*)'' ;
	   ELSE
	      ret := ret || '', 0'' ;
	   END IF;
	   ret := ret || '' AS "'' || r2.feature || ''"'';
       END LOOP ;
       ret := ret || ''
       FROM listarchive 
       WHERE '' || Feature || '' = '''''' || r1.feature || '''''' GROUP BY yearmonth'';
    END LOOP ;

    ret := ret || ''
  ) zw
  GROUP BY yearmonth
  ORDER BY yearmonth;'' ;

    RETURN ret;
  END; ' LANGUAGE 'plpgsql';

/*
 * This query returns stats about all mailing lists
 */

CREATE OR REPLACE FUNCTION BuildQueryCDDs()
    RETURNS text AS '
    DECLARE
       ret        text ;

    BEGIN

    ret := BuildQueryCDDsHelper(
               ''SELECT project AS feature, COUNT(*) AS num FROM listarchive GROUP BY project ORDER BY num DESC;'',
               ''project'') ;
    return ret ;
  END; ' LANGUAGE 'plpgsql';

/*
 * This query returns stats about the ARG2 most active authors in a specific
 * Mailing list (ARG1)
 */

CREATE OR REPLACE FUNCTION BuildQueryAuthors(text, int)
    RETURNS text AS '
    DECLARE
       Project    ALIAS FOR \$1 ;
       NumAuthors ALIAS FOR \$2 ;
       ret        text ;

    BEGIN

    ret := BuildQueryCDDsHelper(
               ''SELECT author AS feature, COUNT(*) AS num FROM listarchive
                 WHERE project = '''''' || Project || '''''' AND author IN (
      SELECT author FROM (SELECT author, count(*) as anz From listarchive where project = '''''' || Project || ''''''
           GROUP BY author ORDER BY anz DESC LIMIT '' || NumAuthors || '') AS zw)
   GROUP BY author ORDER BY num DESC;'',
               ''project = '''''' || Project || '''''' AND author'') ;
    return ret ;
  END; ' LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION CompareCDDs()
    RETURNS SETOF RECORD AS '
    DECLARE
       ret        text ;
       query      text ;
       r          RECORD ;

    BEGIN

    SELECT INTO query BuildQueryCDDs() ;

    FOR r IN EXECUTE query LOOP
        RETURN NEXT r;
    END LOOP;

  END; ' LANGUAGE 'plpgsql';

/*
 * You might call this as, but there is less chance to get column names right.
 *  SELECT * FROM CompareCDDs() AS
 *   ( yearmonth date, cdd1 int, cdd2 int, cdd3 int, cdd4 int, cdd5 int, cdd6 int, cdd7 int, cdd8 int );
 *
 * That's why we use the shell script wrappers ...
 */


/*******************************************
 *
 * Same thing as above but for whole year
 *
 *******************************************/

/*
 * Build a query string for several purposes
 *   ARG1: Query to obtain wanted columns
 *   ARG2: Feature that is queried
 * See below how this helper is used.
 */
CREATE OR REPLACE FUNCTION BuildQueryCDDsYearHelper(text, text)
    RETURNS text AS '
    DECLARE
       IterQuery  ALIAS FOR \$1 ;
       Feature    ALIAS FOR \$2 ;
       ret        text ;
       union      text ;
       query1     text ;
       query2     text ;
       r1         RECORD ;
       r2         RECORD ;
       ri         RECORD ;
       qi         RECORD ;
    BEGIN

    query1 := IterQuery ;
    query2 := query1;
    union  := '''' ;
    ret    := ''SELECT EXTRACT(''''year'''' FROM year) AS year'' ;

    FOR r1 IN EXECUTE query1 LOOP
    	ret := ret || '', CAST(SUM("'' || r1.feature || ''") AS int) AS "'' || r1.feature || ''"'' ;
    END LOOP;

    ret := ret || ''
  FROM (
'' ;

    FOR r1 IN EXECUTE query1 LOOP
       ret   := ret || union || ''    SELECT date_trunc(''''year'''', yearmonth)::date AS year'' ;
       union := ''
    UNION
'';
       FOR r2 IN EXECUTE query2 LOOP
       	   IF r1.feature = r2.feature THEN
	      ret := ret || '', COUNT(*)'' ;
	   ELSE
	      ret := ret || '', 0'' ;
	   END IF;
	   ret := ret || '' AS "'' || r2.feature || ''"'';
       END LOOP ;
       ret := ret || ''
       FROM listarchive 
       WHERE '' || Feature || '' = '''''' || r1.feature || '''''' GROUP BY year'';
    END LOOP ;

    ret := ret || ''
  ) zw
  GROUP BY year
  ORDER BY year;'' ;

    RETURN ret;
  END; ' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION BuildQueryCDDsYear()
    RETURNS text AS '
    DECLARE
       ret        text ;

    BEGIN

    ret := BuildQueryCDDsYearHelper(
               ''SELECT project AS feature, COUNT(*) AS num FROM listarchive GROUP BY project ORDER BY num DESC;'',
               ''project'') ;
    return ret ;
  END; ' LANGUAGE 'plpgsql';

/*
 * This query returns stats about the ARG2 most active authors in a specific
 * Mailing list (ARG1)
 */

CREATE OR REPLACE FUNCTION BuildQueryAuthorsYear(text, int)
    RETURNS text AS '
    DECLARE
       Project    ALIAS FOR \$1 ;
       NumAuthors ALIAS FOR \$2 ;
       ret        text ;

    BEGIN

    ret := BuildQueryCDDsYearHelper(
               ''SELECT author AS feature, COUNT(*) AS num FROM listarchive
                 WHERE project = '''''' || Project || '''''' AND author IN (
      SELECT author FROM (SELECT author, count(*) as anz From listarchive where project = '''''' || Project || ''''''
           GROUP BY author ORDER BY anz DESC LIMIT '' || NumAuthors || '') AS zw)
   GROUP BY author ORDER BY num DESC;'',
               ''project = '''''' || Project || '''''' AND author'') ;
    return ret ;
  END; ' LANGUAGE 'plpgsql';

COMMIT;
EOT

