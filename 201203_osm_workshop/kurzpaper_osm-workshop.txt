Die Teilnehmer des Workshops sollten wenn möglich eigene GPS-Tracks
sowie kommentierende Fotos oder Notizen mitbringen. 
Alternativ sollten die Teilnehmer zumindest eine Vorstellung haben,
welche Informationen sie in OpenStreetMap einpflegen möchten, was auch
anhand eigener Ortskenntnis, mit Hilfe von Satellitenbildern,
fremden GPS-Spuren in OSM, sogenannten "walking papers" oder ähnlichem
geschehen kann. Sollte kein entsprechendes Material zur Verfügung
stehen, wird etwas durch die Mentoren des Workshops bereitgestellt.

Es ist wünschenswert, dass die Workshopteilnehmer im Vorfeld bereits
einen Account unter http://osm.org/user/new anlegen.

Mitzubringen ist ein Laptop mit aktuellem Java und möglichst bereits
installiertem JOSM. Im Workshop werden empfehlenswerte Plugins
installiert. Eine externe Maus erleichtert die Bedienung.

Im Workshop werden die Teilnehmer in die Bedienung des OSM-Editors JOSM
eingeführt. Ziel des Workshops ist, daß jeder Teilnehmer
einen echten Edit in der OSM Datenbank durchführt und das Ergebnis im
OSM Kartenmaterial begutachtet.

Dabei wird erläutert, wie man nach Tags sucht und welche Fehler zu
vermeiden sind.

Es gibt eine Wiki-Seite

   http://malenki.ch/clt_2012/ws

für den OSM-Auftritt zu den CLT.  Dort werden weitere Informationen,
Links etc. zu finden sein.

